use rand::{thread_rng, Rng};
use clap::{App, Arg};

fn main() {
    let matches = App::new("pwgen")
        .arg(
            Arg::with_name("length")
            .help("Password length")
            .takes_value(true)
            .required(false)
            .default_value("8")
            .short("l")
            .validator(is_number)
            )
        .get_matches();
    let pwlength = matches.value_of("length").unwrap();
    const CHARSET: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                            abcedfghijklmnopqrstuvwxyz\
                            01234556789)(*&*%$#@!~";
    let mut rng = rand::thread_rng();

    let password: String = (0..pwlength.parse::<u8>().unwrap())
        .map(|_| {
            let idx = rng.gen_range(0..CHARSET.len());
            CHARSET[idx] as char
        })
    .collect();

    println!("{}", password);
}

fn is_number(val: String) -> Result<(), String> {
    if val.chars().next().unwrap().is_numeric() {
        Ok(())
    }
    else {
        Err(val)
    }
}